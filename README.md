# README #
This is a Connect4 application using Socket.io; Players can play on the same network on different devices with the given url.

### How to ###
To get up and running, use 

```
#!node

$ npm install
```


followed by 

```
#!node

$ node server
```

this will set up a server on port http://localhost:8000


![Screenshot 2015-02-26 13.20.20.PNG](https://bitbucket.org/repo/B8bXeL/images/360559748-Screenshot%202015-02-26%2013.20.20.PNG)